package com.redlipsjournal.arch.module

import android.app.Application
import com.redlipsjournal.ThisApplication
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module
class ApplicationModule(val application: ThisApplication) {

    @Provides
    internal fun provideThisApplication(): ThisApplication {
        return application
    }

    @Provides
    internal fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    fun provideEventBus(): EventBus {
        return EventBus.getDefault()
    }
}