package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.arch.repositories.ArticleRepository
import java.util.*


class ColumnViewModel(private val apiRepository: ArticleRepository) : ViewModel() {

    private val columnsData= MutableLiveData<List<Category>>()

    fun subscribe(): LiveData<List<Category>> = columnsData

    fun getColumns(){
        apiRepository.getColumns(object : ArticleRepository.LoadDataCallback<List<Category>> {
            override fun onDataLoaded(results: List<Category>) {
                columnsData.value = results
            }

            override fun onDataNotAvailable() {
                columnsData.value = Collections.emptyList()
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        apiRepository.stopRequestColumns()
    }
}