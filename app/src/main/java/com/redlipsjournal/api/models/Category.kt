package com.redlipsjournal.api.models

import com.google.gson.annotations.SerializedName

class Category {

    @SerializedName(ID)
    val id: String? = null
    @SerializedName(TITLE)
    val title: String? = null
    @SerializedName(DESCRIPTION)
    val description: String? = null
    @SerializedName(CATEGORY_AUTHOR)
    val author: Author? = null
    @SerializedName(SLUG)
    val slug: String? = null
    @SerializedName(CATEGORY_TYPE)
    val categoryType: String? = null
    @SerializedName(ARTICLES_COUNT)
    val articlesCount: Int? = null

    companion object {
        const val ID = "id"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val CATEGORY_AUTHOR = "category_author"
        const val SLUG = "slug"
        const val CATEGORY_TYPE = "category_type"
        const val ARTICLES_COUNT = "articles_count"
    }

    override fun toString(): String {
        return title.toString()
    }
}
