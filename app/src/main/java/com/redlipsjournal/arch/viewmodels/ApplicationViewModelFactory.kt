package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.redlipsjournal.arch.repositories.ArticleRepository
import com.redlipsjournal.managers.FavoritesManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApplicationViewModelFactory
@Inject constructor(private val apiRepository: ArticleRepository, val favoritesManager: FavoritesManager)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(CategoryViewModel::class.java) -> CategoryViewModel(apiRepository) as T
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> HomeViewModel(apiRepository) as T
            modelClass.isAssignableFrom(SplashViewModel::class.java) -> SplashViewModel(apiRepository) as T
            modelClass.isAssignableFrom(ColumnViewModel::class.java) -> ColumnViewModel(apiRepository) as T
            modelClass.isAssignableFrom(PreferencesViewModel::class.java) -> PreferencesViewModel(favoritesManager) as T
            modelClass.isAssignableFrom(ArticleDetailViewModel::class.java) -> ArticleDetailViewModel(apiRepository) as T
            else -> throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}