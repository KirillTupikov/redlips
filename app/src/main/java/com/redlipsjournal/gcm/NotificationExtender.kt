package com.redlipsjournal.gcm

import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import com.redlipsjournal.R

class NotificationExtender : NotificationExtenderService() {
    override fun onNotificationProcessing(receivedResult: OSNotificationReceivedResult): Boolean {
//        val data = receivedResult.payload.additionalData

        if (false) { //TODO check enable notifications
            val overrideSettings = NotificationExtenderService.OverrideSettings()
            overrideSettings.extender = NotificationCompat.Extender { builder ->
                builder
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                        .setColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
            }

            val result = displayNotification(overrideSettings)

            return false
        } else {
            return true
        }
    }
}
