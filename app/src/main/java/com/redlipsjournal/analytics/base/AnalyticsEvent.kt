package com.redlipsjournal.analytics.base

class AnalyticsEvent(val type: Type, val id: String, val payload: Map<String, String> = mapOf()) {
    enum class Type {
        SCREEN, EVENT, ACTION
    }
}