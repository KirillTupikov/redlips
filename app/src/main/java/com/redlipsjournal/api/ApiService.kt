package com.redlipsjournal.api

import com.redlipsjournal.api.models.Article
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("/api/article?post_status=0")
    fun getArticleList(@Query("categories") categoryId: String): Single<String>

    @GET("/api/article?featured=2")
    fun getHomeArticleList(): Single<String>

    @GET("/api/category?column=3")
    fun getCategoryList(): Single<String>

    @GET("/api/category?column=2")
    fun getColumns(): Single<String>

    @GET("/api/article/{id}")
    fun getArticleById(@Path("id") id: String): Single<String>
}