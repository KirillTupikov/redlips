package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.ArticleResponse
import com.redlipsjournal.arch.repositories.ArticleRepository
import org.json.JSONObject
import java.util.*

class ArticleDetailViewModel(private val apiRepository: ArticleRepository) : ViewModel() {

    private val articlesData = MutableLiveData<Article>()

    fun subscribeArticle(): LiveData<Article> = articlesData

    fun getArticleById(articleId: String) {
        apiRepository.getArticleById(articleId, object : ArticleRepository.LoadDataCallback<Article> {
            override fun onDataLoaded(results: Article) {
                articlesData.value = results
            }

            override fun onDataNotAvailable() {
                articlesData.value = Article(JSONObject())
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        apiRepository.stopRequestArticle()
    }
}