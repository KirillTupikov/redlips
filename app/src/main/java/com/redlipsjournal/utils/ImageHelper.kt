package com.redlipsjournal.utils

import android.graphics.Bitmap
import androidx.annotation.DrawableRes
import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.arch.module.NetworkModule
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

object ImageHelper {

    private const val CROSS_FADE_DURATION = 500
    private const val CROSS_FADE_DURATION_FAST = 150

    private fun getGlideUrl(url:String):String{
        return NetworkModule.BASE_URL + url
    }

    private fun isWrongUrl(url: String): Boolean {
        return TextUtils.isEmpty(url) || TextUtils.equals("null", url)
    }

    fun loadImage(imageView: ImageView, url: String, @DrawableRes errorImageRes: Int = 0, @DrawableRes placeholderId: Int = 0) {
        if (isWrongUrl(url)) {
            if (placeholderId != 0) {
                imageView.setImageResource(placeholderId)
            }
            return
        }

        val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .fitCenter()
                .centerCrop()

        if (placeholderId != 0) requestOptions.placeholder(placeholderId)
        if (errorImageRes != 0) requestOptions.error(errorImageRes)

        GlideApp.with(imageView.context)
                .asBitmap()
                .load(getGlideUrl(url))
                .apply(requestOptions)
//                .transition(DrawableTransitionOptions.withCrossFade(CROSS_FADE_DURATION))
                .into(imageView)
    }

    fun loadImageWithPlaceholder(imageView: ImageView, url: String, @DrawableRes placeholderId: Int= 0) {
        if (isWrongUrl(url)) {
            if (placeholderId != 0) {
                imageView.setImageResource(placeholderId)
            }
            return
        }

        val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .centerCrop()

        if (placeholderId != 0) requestOptions.placeholder(placeholderId)

        GlideApp.with(imageView.context)
                .load(getGlideUrl(url))
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade(CROSS_FADE_DURATION))
                .into(imageView)
    }

    fun loadNotCropImage(imageView: ImageView, url: String) {
        if (isWrongUrl(url)) {
            imageView.setImageResource(R.drawable.placeholder_picture)
            return
        }

        val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .fitCenter()
                .placeholder(R.drawable.placeholder_picture)

        GlideApp.with(imageView.context)
                .load(getGlideUrl(url))
                .apply(requestOptions)
                .transition(DrawableTransitionOptions.withCrossFade(CROSS_FADE_DURATION))
                .into(imageView)
    }

    fun saveImageToFile(bitmap: Bitmap, name: String): File {
        val filesDir = ThisApplication.instance?.cacheDir
        val imageFile = File(filesDir, "$name.jpg")

        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
            os.flush()
            os.close()
        } catch (e: Exception) {
            Log.e(javaClass.simpleName, "Error writing bitmap", e)
        }

        return imageFile
    }
}
