package com.redlipsjournal.ui

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.SplashViewModel
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseActivity
import com.redlipsjournal.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mSplashViewModel: SplashViewModel

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.SplashScreen.serialize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ThisApplication.instance?.appComponent?.inject(this)

        mSplashViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(SplashViewModel::class.java)

        mSplashViewModel.subscribeCategory().observe(this, Observer { categories ->
            categories?.let { list ->
                if (!list.isEmpty()) {

                    mMediaDataManager.categories = list

                    HomeActivity.start(this)
                } else {
                    finish()
                    Toast.makeText(this, getString(R.string.splash_error), LENGTH_SHORT).show()
                }
            }
        })

        val scaleDownX = ObjectAnimator.ofFloat(splash_back, "scaleX", 1.02f)
        val scaleDownY = ObjectAnimator.ofFloat(splash_back, "scaleY", 1.02f)
        scaleDownX.duration = 4000
        scaleDownY.duration = 4000

        val scaleDown = AnimatorSet()
        scaleDown.play(scaleDownX).with(scaleDownY)

        scaleDown.start()
    }

    override fun onResume() {
        super.onResume()
        mSplashViewModel.getCategory()
    }
}
