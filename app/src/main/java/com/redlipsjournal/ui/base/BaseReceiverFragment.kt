package com.redlipsjournal.ui.base

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager

abstract class BaseReceiverFragment : BaseFragment() {


    override fun onResume() {
        super.onResume()
        if (!isHidden) {
            registerReceiver()
        }
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver()
    }

    private fun unregisterReceiver() {
        networkReceiver.isFirstTime = true
        try {
            activity?.unregisterReceiver(networkReceiver)
        } catch (ex: Exception) {
        }
    }

    private fun registerReceiver() {
        networkReceiver.isFirstTime = true
        try {
            activity?.registerReceiver(networkReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        } catch (ex: Exception) {
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) unregisterReceiver() else
            registerReceiver()
    }

    abstract fun onConnectionChanged(isConnection: Boolean)

    private val networkReceiver = object : BroadcastReceiver() {

        var isFirstTime = true

        override fun onReceive(context: Context?, intent: Intent?) {
            if (isFirstTime) {
                isFirstTime = false
                return
            }

            onConnectionChanged((context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)
                    ?.activeNetworkInfo?.isConnected ?: false)
        }
    }
}