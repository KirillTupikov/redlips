package com.redlipsjournal.api;

import android.util.Log;

import com.google.gson.Gson;
import com.redlipsjournal.api.models.Article;
import com.redlipsjournal.api.models.ArticleResponse;
import com.redlipsjournal.api.models.Category;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

public class Parser {

    private static final String TAG = Parser.class.getSimpleName();

    public static Function<String, List<Category>> parseCategories(Gson gson) {
        return s -> {
            List<Category> categories = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray catArray = jsonObject.getJSONArray("results");
                if (catArray != null) {
                    for (int i = 0; i < catArray.length(); i++) {
                        JSONObject categoryJson = catArray.getJSONObject(i);
                        Category category = gson.fromJson(categoryJson.toString(), Category.class);
                        categories.add(category);
                    }
                }

            } catch (Exception e) {
                Log.e(TAG, "Parse categories json error", e);
            }

            return categories;
        };
    }

    public static Function<String, ArticleResponse> parseArticles(Gson gson) {
        return s -> {
            try {
                return gson.fromJson(s, ArticleResponse.class);

            } catch (Exception e) {
                Log.e(TAG, "Parse articles json error", e);
            }

            return new ArticleResponse();
        };
    }

    public static Function<String, Article> parseArticle(Gson gson) {
        return s -> {
            try {
                return gson.fromJson(s, Article.class);

            } catch (Exception e) {
                Log.e(TAG, "Parse article json error", e);
            }

            return new Article(new JSONObject());
        };
    }
}
