package com.redlipsjournal.arch.repositories

import android.text.format.DateUtils
import com.google.gson.Gson
import com.redlipsjournal.api.ApiService
import com.redlipsjournal.api.Parser
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.ArticleResponse
import com.redlipsjournal.api.models.Category
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ArticleRepository @Inject constructor(val apiService: ApiService, val gson: Gson) {

    private var disposableCategories: Disposable? = null
    private var disposableArticles: Disposable? = null
    private var disposableHomeArticles: Disposable? = null
    private var disposableColumns: Disposable? = null
    private var disposableArticle: Disposable? = null

    fun getCategories(callback: LoadDataCallback<List<Category>>?) {
        disposableCategories = apiService
                .getCategoryList()
                .map(Parser.parseCategories(gson))
                .map {
                    try {
                        Thread.sleep(((DateUtils.SECOND_IN_MILLIS * 1.5).toLong()))
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    it
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Category>>() {
                    override fun onSuccess(responce: List<Category>) {
                        callback?.onDataLoaded(responce)
                    }

                    override fun onError(e: Throwable) {
                        callback?.onDataNotAvailable()
                    }
                })
    }

    fun getArticles(categoryId: String, callback: LoadDataCallback<ArticleResponse>?) {
        disposableArticles = apiService
                .getArticleList(categoryId)
                .map(Parser.parseArticles(gson))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ArticleResponse>() {
                    override fun onSuccess(responce: ArticleResponse) {
                        callback?.onDataLoaded(responce)
                    }

                    override fun onError(e: Throwable) {
                        callback?.onDataNotAvailable()
                    }
                })
    }

    fun getHomeArticles(callback: LoadDataCallback<ArticleResponse>?) {
        disposableHomeArticles = apiService
                .getHomeArticleList()
                .map(Parser.parseArticles(gson))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ArticleResponse>() {
                    override fun onSuccess(responce: ArticleResponse) {
                        callback?.onDataLoaded(responce)
                    }

                    override fun onError(e: Throwable) {
                        callback?.onDataNotAvailable()
                    }
                })
    }

    fun getColumns(callback: LoadDataCallback<List<Category>>?) {
        disposableColumns = apiService
                .getColumns()
                .map(Parser.parseCategories(gson))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Category>>() {
                    override fun onSuccess(categoties: List<Category>) {
                        callback?.onDataLoaded(categoties)
                    }

                    override fun onError(e: Throwable) {
                        callback?.onDataNotAvailable()
                    }
                })
    }

    fun getArticleById(id: String, callback: LoadDataCallback<Article>?) {
        disposableArticle = apiService
                .getArticleById(id)
                .map(Parser.parseArticle(gson))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Article>() {
                    override fun onSuccess(article: Article) {
                        callback?.onDataLoaded(article)
                    }

                    override fun onError(e: Throwable) {
                        callback?.onDataNotAvailable()
                    }
                })
    }

    fun stopRequestHomeArticles() {
        disposableHomeArticles?.dispose()
    }

    fun stopRequestArticles() {
        disposableArticles?.dispose()
    }

    fun stopRequestCategories() {
        disposableCategories?.dispose()
    }

    fun stopRequestColumns() {
        disposableColumns?.dispose()
    }

    fun stopRequestArticle() {
        disposableArticle?.dispose()
    }

    interface LoadDataCallback<T> {

        fun onDataLoaded(results: T)

        fun onDataNotAvailable()
    }
}