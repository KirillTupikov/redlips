package com.redlipsjournal.ui.home.category

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.HomeViewModel
import com.redlipsjournal.managers.FavoritesManager
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseReceiverFragment
import com.redlipsjournal.utils.EventBusHelper
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import javax.inject.Inject

open class HomeFragment : BaseReceiverFragment() {

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    @Inject
    lateinit var mFavoritesManager: FavoritesManager

    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mHomeViewModel: HomeViewModel

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.HomeScreen.serialize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThisApplication.instance?.appComponent?.inject(this)

        mHomeViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(HomeViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ArticleAdapter(mFavoritesManager, mMediaDataManager)
        activity?.let { adapter.setActivity(it) }
        recyclerView.adapter = adapter

        mHomeViewModel.subscribeArticles().observe(this, Observer { articles ->
            val dataList = ArrayList<Any>()

            articles?.let { dataList.addAll(it) }

            (recyclerView.adapter as ArticleAdapter).setData(dataList)
            if (dataList.isNotEmpty()) {
                showContent()
            }
        })

        mHomeViewModel.getHomeArticles()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(preferenceEvent: EventBusHelper.PreferenceChangedEvent) {
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun showProgress() {
        viewFlipper.displayedChild = 0
    }

    private fun showContent() {
        viewFlipper.displayedChild = 1
    }

    override fun onConnectionChanged(isConnection: Boolean) {
        recyclerView.adapter?.let { adapter ->
            if (adapter.itemCount > 0) {
                val toast = Toast.makeText(context, getString(if (isConnection) R.string.found_connection
                else R.string.connection_lost), Toast.LENGTH_SHORT)

                val styledAttributes = context?.theme?.obtainStyledAttributes(
                        intArrayOf(android.R.attr.actionBarSize))

                toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0,
                        styledAttributes?.getDimension(0, 0f)?.toInt() ?: 0)

                toast.show()
                styledAttributes?.recycle()
            } else if (isConnection) {
                showProgress()
                mHomeViewModel.getHomeArticles()
            } else {
                viewFlipper.displayedChild = 2
            }
        }
    }

    companion object {

        fun newInstance(): HomeFragment {

            val args = Bundle()

            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
