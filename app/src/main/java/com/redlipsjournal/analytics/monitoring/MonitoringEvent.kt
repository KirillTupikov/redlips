package com.redlipsjournal.analytics.monitoring

import com.redlipsjournal.analytics.base.AnalyticsEvent

sealed class MonitoringEvent(
        private val event: Event,
        private val payload: Map<String, String> = mapOf()
) {
    fun serialize(): AnalyticsEvent {
        return AnalyticsEvent(AnalyticsEvent.Type.EVENT, event.toString(), payload)
    }

    object ClickShareArticle : MonitoringEvent(Event.SHARE_ARTICLE.apply {
        addScreen(Screen.ARTICLE_DETAIL)
        addType(TYPE_BUTTON)
    })

    object ClickAddFavorites : MonitoringEvent(Event.ADD_FAVORITES.apply {
        addType(TYPE_BUTTON)
    })

    object ClickRemoveFavorites : MonitoringEvent(Event.REMOVE_FAVORITES.apply {
        addType(TYPE_BUTTON)
    })

    companion object {
        private const val TYPE_BUTTON = "button"
        private const val TYPE_NOTIFICATION = "notification"
    }
}

enum class Event(val id: String) {
    SHARE_ARTICLE("share_article"),
    ADD_FAVORITES("add_favorites"),
    REMOVE_FAVORITES("remove_favorites"),

    NOTIFICATIONS("daily");

    private var screen: Screen? = null
    fun addScreen(screen: Screen) {
        this.screen = screen
    }

    private var type: String = ""
    fun addType(type: String) {
        this.type = type
    }

    override fun toString(): String {
        val prefix = if (type.isNotEmpty()) "${type}_" else ""

        screen?.let { theScreen -> // button_login_{id]
            return "$prefix${theScreen.id}_$id"
        }

        return "$prefix$id"
    }
}