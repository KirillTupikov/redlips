package com.redlipsjournal.ui.home

interface OnMenuDisabledListener {
    fun uncheckedAll()
}