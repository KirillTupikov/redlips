package com.redlipsjournal.ui.home.column

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.ColumnViewModel
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseReceiverFragment
import com.redlipsjournal.ui.home.column.adapter.ColumnAdapter
import com.redlipsjournal.ui.view.MenuDividerDecoration
import kotlinx.android.synthetic.main.fragment_columns.*
import javax.inject.Inject

class ColumnFragment : BaseReceiverFragment() {

    companion object {

        fun newInstance(): ColumnFragment {

            val args = Bundle()

            val fragment = ColumnFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mColumnViewModel: ColumnViewModel

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    @Inject
    override fun getLayoutId(): Int {
        return R.layout.fragment_columns
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.ColumnsScreen.serialize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThisApplication.instance?.appComponent?.inject(this)

        mColumnViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ColumnViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        columnsRecyclerView.adapter = ColumnAdapter()

        context?.let {
            columnsRecyclerView.addItemDecoration(MenuDividerDecoration(it))
        }

        mColumnViewModel.subscribe().observe(this, Observer { columns ->
            columns?.let { columnsList ->
                mMediaDataManager.columns = columnsList

                (columnsRecyclerView.adapter as? ColumnAdapter)?.setData(columnsList)
                if (columnsList.isNotEmpty()) {
                    showContent()
                }
            }
        })

        showProgress()
        mColumnViewModel.getColumns()
    }

    private fun showProgress() {
        viewFlipper.displayedChild = 0
    }

    private fun showContent() {
        viewFlipper.displayedChild = 1
    }

    override fun onConnectionChanged(isConnection: Boolean) {
        mMediaDataManager.columns?.let { colunms ->
            if (colunms.size > 0) {
                val toast = Toast.makeText(context, getString(if (isConnection) R.string.found_connection
                else R.string.connection_lost), Toast.LENGTH_SHORT)

                val styledAttributes = context?.theme?.obtainStyledAttributes(
                        intArrayOf(android.R.attr.actionBarSize))

                toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0,
                        styledAttributes?.getDimension(0, 0f)?.toInt() ?: 0)
                toast.show()
                styledAttributes?.recycle()
            } else if (isConnection) {
                showProgress()
                mColumnViewModel.getColumns()
            } else {
                viewFlipper.displayedChild = 2
            }
        }
    }
}
