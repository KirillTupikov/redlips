package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.managers.FavoritesManager

class PreferencesViewModel(private val favoritesManager: FavoritesManager) : ViewModel() {

    private val articlesData = MutableLiveData<List<Article>>()

    fun subscribePreferences(): LiveData<List<Article>> = articlesData

    fun getArticles() {
        articlesData.value = favoritesManager.getArticlesList()
    }
}