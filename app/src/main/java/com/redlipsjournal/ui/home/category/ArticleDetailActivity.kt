package com.redlipsjournal.ui.home.category

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.widget.NestedScrollView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.widget.FrameLayout
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.AnalyticsManager
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.ArticleDetailViewModel
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseWebActivity
import com.redlipsjournal.utils.HtmlHelper
import com.redlipsjournal.utils.ImageHelper
import kotlinx.android.synthetic.main.activity_article_detail.*
import javax.inject.Inject

class ArticleDetailActivity : BaseWebActivity() {

    override fun getScrollView(): NestedScrollView {
        return scroll_layout
    }

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout {
        return list_refresh_layout
    }

    override fun getWVContainer(): FrameLayout {
        return web_view_container
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.ArticleDetail.serialize()
    }

    @Inject
    lateinit var mMediaDataManager: MediaDataManager
    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mArticleDetailViewModel: ArticleDetailViewModel

    private var mSelectArticle: Article? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_article_detail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        ThisApplication.instance?.appComponent?.inject(this)
        mArticleDetailViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ArticleDetailViewModel::class.java)

        mArticleDetailViewModel.subscribeArticle().observe(this, Observer { article ->
            mSelectArticle = article
            fillContent()
        })

        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initView() {
        enableTitle(false)
        setEnabledHome(true)

        mSelectArticle = mMediaDataManager.selectArticle
        if (mSelectArticle?.html == null) {
            mSelectArticle?.id?.let { mArticleDetailViewModel.getArticleById(it) }
        } else {
            fillContent()
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            val extras = intent.extras
            val imageTransitionName = extras?.getString(IMAGE_TRANSITION_NAME)
            val titleTransitionName = extras?.getString(TITLE_TRANSITION_NAME)
            val authorTransitionName = extras?.getString(AUTHOR_TRANSITION_NAME)
            val countTransitionName = extras?.getString(COUNT_TRANSITION_NAME)
            val containerTransitionName = extras?.getString(CONTAINER_TRANSITION_NAME)

            article_image.transitionName = imageTransitionName
            author.transitionName = authorTransitionName
            count_view.transitionName = countTransitionName
            article_name.transitionName = titleTransitionName
            container_view.transitionName = containerTransitionName

            startPostponedEnterTransition()
        }
    }

    private fun fillContent() {
        ImageHelper.loadImageWithPlaceholder(article_image, mSelectArticle?.postThumbnail?.file.toString())
        author.text = mSelectArticle?.author?.displayName
        count_view.text = mSelectArticle?.postViews
        article_name.text = mSelectArticle?.title
        loadWebData(HtmlHelper.optimizedHtml(this, mSelectArticle?.html!!))
    }

    override fun setSettings(webView: WebView?) {

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.article_details, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_share -> {
                AnalyticsManager.getInstance()?.track(MonitoringEvent.ClickShareArticle.serialize())

                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                sendIntent.putExtra(Intent.EXTRA_TEXT, mSelectArticle?.articleSourceUrl)
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isFullscreen) {
            hideVideoView()
            return
        }

        if (mWebView.canGoBack()) {
            mWebView.goBack()
            return
        }

        super.onBackPressed()
    }

    companion object {

        const val IMAGE_TRANSITION_NAME = "IMAGE_TRANSITION_NAME"
        const val TITLE_TRANSITION_NAME = "TITLE_TRANSITION_NAME"
        const val AUTHOR_TRANSITION_NAME = "AUTHOR_TRANSITION_NAME"
        const val COUNT_TRANSITION_NAME = "COUNT_TRANSITION_NAME"
        const val CONTAINER_TRANSITION_NAME = "CONTAINER_TRANSITION_NAME"

        fun start(context: Context) {
            val starter = Intent(context, ArticleDetailActivity::class.java)
            context.startActivity(starter)
        }
    }
}
