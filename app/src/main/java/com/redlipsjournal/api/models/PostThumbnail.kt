package com.redlipsjournal.api.models

import com.google.gson.annotations.SerializedName

class PostThumbnail {

    @SerializedName(ID)
    val id: String? = null
    @SerializedName(HEADER_THUMBNAIL)
    val headerThumbnail: String? = null
    @SerializedName(TITLE)
    val title: String? = null
    @SerializedName(FILE)
    var file: String? = null
    @SerializedName(UPLOADED_BY_USER)
    val uploadedByUser: String? = null

    companion object {

        const val ID = "id"
        const val HEADER_THUMBNAIL = "header_thumbnail"
        const val TITLE = "title"
        const val FILE = "file"
        const val UPLOADED_BY_USER = "uploaded_by_user"
    }
}
