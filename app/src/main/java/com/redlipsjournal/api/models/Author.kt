package com.redlipsjournal.api.models

import com.google.gson.annotations.SerializedName

class Author {

    @SerializedName(ID)
    val id: String? = null
    @SerializedName(VK_URL)
    val vkUrl: String? = null
    @SerializedName(DISPLAY_NAME)
    var displayName: String? = null
    @SerializedName(THUMBNAIL)
    val thumbnail: String? = null
    @SerializedName(DESCRIPTION)
    val description: String? = null
    @SerializedName(IDENTIFY)
    val identify: String? = null
    @SerializedName(THUMBNAIL_2X)
    val thumbnail2x: String? = null
    @SerializedName(ROLES)
    val roles: Array<String>? = null
    @SerializedName(INSTAGRAM_URL)
    val instagramUrl: String? = null
    @SerializedName(WEBSITE_URL)
    val websiteUrl: String? = null
    @SerializedName(AVATAR)
    val avatar: String? = null
    @SerializedName(FACEBOOK_URL)
    val facebookUrl: String? = null
    @SerializedName(URL)
    val url: String? = null

    companion object {

        const val ID = "id"
        const val VK_URL = "vk_url"
        const val DISPLAY_NAME = "display_name"
        const val THUMBNAIL = "thumbnail"
        const val DESCRIPTION = "description"
        const val IDENTIFY = "identify"
        const val THUMBNAIL_2X = "thumbnail_2x"
        const val ROLES = "roles"
        const val INSTAGRAM_URL = "instagram_url"
        const val WEBSITE_URL = "website_url"
        const val AVATAR = "avatar"
        const val FACEBOOK_URL = "facebook_url"
        const val URL = "url"
    }

    override fun toString(): String {
        return displayName.toString()
    }
}
