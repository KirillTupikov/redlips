package com.redlipsjournal.api.models

import com.google.gson.annotations.SerializedName

class ArticleResponse {

    @SerializedName(PREVIOUS_PAGE)
    val previousPage: Int = 0
    @SerializedName(NEXT_PAGE)
    val nextPage: Int = 0
    @SerializedName(COUNT)
    val count: Int = 0
    @SerializedName(ARTICLES)
    val articles: List<Article>? = null

    companion object {

        const val PREVIOUS_PAGE = "previous_page_number"
        const val NEXT_PAGE = "next_page_number"
        const val COUNT = "count"
        const val ARTICLES = "results"
    }
}
