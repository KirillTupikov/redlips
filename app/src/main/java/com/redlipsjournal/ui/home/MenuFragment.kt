package com.redlipsjournal.ui.home

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseFragment
import com.redlipsjournal.ui.base.BaseRecyclerViewAdapter
import com.redlipsjournal.ui.view.MenuDividerDecoration
import kotlinx.android.synthetic.main.fragment_menu.*
import javax.inject.Inject

class MenuFragment : BaseFragment(), OnMenuDisabledListener {

    var mSelectPosition = 0

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    var mOnMenuClickListener: OnMenuClickListener? = null

    companion object {

        fun newInstance(): MenuFragment {

            val args = Bundle()

            val fragment = MenuFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_menu
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThisApplication.instance?.appComponent?.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity is OnMenuClickListener) {
            mOnMenuClickListener = activity as OnMenuClickListener
        }

        context?.let {
            recyclerView.addItemDecoration(MenuDividerDecoration(it))
        }

        val adapter = MenuAdapter()
        adapter.setData(mMediaDataManager.categories)

        recyclerView.adapter = adapter
    }

    override fun uncheckedAll() {
        val pos = mSelectPosition
        mSelectPosition = -1
        if (pos != -1) {
            recyclerView?.adapter?.notifyItemChanged(pos)
        }
    }

    inner class MenuAdapter : BaseRecyclerViewAdapter<Category, MenuAdapter.CategoryViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_category_view, parent, false)
            return CategoryViewHolder(view)
        }

        override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {

            val category = mData[position]

            holder.name.text = category.title
            holder.count.text = category.articlesCount.toString()

            holder.container.setBackgroundResource(if (position == mSelectPosition) R.color.colorSelectMenuItem else R.color.transparent)

            holder.itemView.setOnClickListener {
                mOnMenuClickListener?.onMenuClick(category, position)
                mSelectPosition = position

                notifyDataSetChanged()
            }
        }

        inner class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            val container: View = view.findViewById(R.id.container)
            val name: TextView = view.findViewById(R.id.category_name)
            val count: TextView = view.findViewById(R.id.article_count)
        }
    }

    interface OnMenuClickListener {
        fun onMenuClick(category: Category, position: Int)
    }
}
