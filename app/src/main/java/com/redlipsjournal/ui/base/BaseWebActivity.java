package com.redlipsjournal.ui.base;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.redlipsjournal.R;
import com.redlipsjournal.arch.module.NetworkModule;

public abstract class BaseWebActivity extends BaseActivity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    protected WebView mWebView;

    private String mFinalHtml;

    private boolean mIsFullscreen;
    private WebChromeClient.CustomViewCallback mViewCallback;
    private ViewGroup mRootLayout;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSwipeRefreshLayout().setColorSchemeResources(R.color.colorAccent);

        mRootLayout = (ViewGroup) getWindow().getDecorView();

        createWebView();
        initView();
    }

    protected abstract NestedScrollView getScrollView();

    protected abstract SwipeRefreshLayout getSwipeRefreshLayout();

    protected abstract FrameLayout getWVContainer();

    protected abstract void initView();

    protected abstract void setSettings(WebView webView);

    @SuppressLint("SetJavaScriptEnabled")
    private void createWebView() {
        mWebView = new WebView(this);
        getWVContainer().addView(mWebView);

        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setVerticalScrollBarEnabled(true);
        mWebView.setHorizontalScrollBarEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        mWebView.setWebChromeClient(new CustomWebChromeClient());
        mWebView.setWebViewClient(new AppWebClient());

        mWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void imageClick(String url) {
//                PhotoDetailActivity.start(getContext(), url);
            }
        }, "Android");

        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });

        setSettings(mWebView);
    }

    public void loadWebData(String html) {
        mFinalHtml = html;
        mWebView.loadDataWithBaseURL(NetworkModule.BASE_URL, mFinalHtml, "text/html", "utf-8", null);
    }

    protected void showSwitchProgress(boolean show) {
        if (show) {
            getSwipeRefreshLayout().setRefreshing(true);
            getScrollView().setVisibility(View.INVISIBLE);
        } else {
            getSwipeRefreshLayout().setRefreshing(false);
            getSwipeRefreshLayout().setEnabled(false);
            getScrollView().setVisibility(View.VISIBLE);
        }
    }

    public void pauseWebView() {
        if (mWebView != null) {
            mWebView.onPause();
        }
    }

    public void startWebView() {
        if (mWebView != null) {
            mWebView.onResume();
        }
    }

    private class AppWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            getSwipeRefreshLayout().setRefreshing(true);
            getScrollView().setVisibility(View.INVISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            getSwipeRefreshLayout().setRefreshing(false);
            getSwipeRefreshLayout().setEnabled(false);
            getScrollView().setVisibility(View.VISIBLE);
            getScrollView().scrollTo(0, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewCallback = null;
        getWVContainer().removeAllViews();
        mWebView.destroy();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mViewCallback != null) {
            mViewCallback.onCustomViewHidden();
            mViewCallback = null;
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if (mViewCallback != null) {
            mViewCallback.onCustomViewHidden();
            mViewCallback = null;
        }
        return true;
    }

    public class CustomWebChromeClient extends WebChromeClient {

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            super.onShowCustomView(view, callback);
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mIsFullscreen = true;
            mViewCallback = callback;
            if (view instanceof FrameLayout) {
                setupVideoLayout(view);
            }
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onHideCustomView() {
            hideVideoView();
        }
    }

    private FrameLayout mVideoLayout;
    private WindowManager.LayoutParams mWindowLayoutParams;
    private int mOldWidth;
    private int mOldHeight;

    private void setupVideoLayout(View video) {
        mVideoLayout = new FrameLayout(this) {

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    hideVideoView();
                    return true;
                }

                return super.dispatchKeyEvent(event);
            }
        };

        mVideoLayout.setBackgroundResource(android.R.color.black);
        mVideoLayout.addView(video);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mRootLayout.addView(mVideoLayout, lp);
        mIsFullscreen = true;

        mRootLayout.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
        );

        mWindowLayoutParams = getWindow().getAttributes();

        mOldWidth = mWindowLayoutParams.width;
        mOldHeight = mWindowLayoutParams.height;

        mWindowLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        mWindowLayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        getWindow().setAttributes(mWindowLayoutParams);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public void hideVideoView() {
        if (isFullscreen()) {
            mRootLayout.removeView(mVideoLayout);
            mViewCallback.onCustomViewHidden();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            mIsFullscreen = false;
        }

        mWindowLayoutParams.width = mOldWidth;
        mWindowLayoutParams.height = mOldHeight;

        getWindow().setAttributes(mWindowLayoutParams);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public boolean isFullscreen() {
        return mIsFullscreen;
    }
}
