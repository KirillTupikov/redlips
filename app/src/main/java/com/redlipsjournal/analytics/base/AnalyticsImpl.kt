package com.redlipsjournal.analytics.base

import android.app.Activity

class AnalyticsImpl(firebase: MonitoringDriver? = null) : Analytics {

    private val drivers by lazy {
        arrayListOf<MonitoringDriver>().apply {
            firebase?.let { this.add(it) }
        }
    }

    override fun screen(activity: Activity, screen: String, data: Map<String, String>) {
        drivers.forEach { driver ->
            driver.screen(activity, screen, data)
        }
    }

    override fun screen(screen: String, data: Map<String, String>) {
        drivers.forEach { driver ->
            driver.screen(screen, data)
        }
    }

    override fun action(action: String, data: Map<String, String>) {
        drivers.forEach { driver ->
            driver.action(action, data)
        }
    }

    override fun event(event: String, data: Map<String, String>) {
        drivers.forEach { driver ->
            driver.event(event, data)
        }
    }
}