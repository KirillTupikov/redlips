package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.ArticleResponse
import com.redlipsjournal.arch.repositories.ArticleRepository
import java.util.*

class CategoryViewModel(private val apiRepository: ArticleRepository) : ViewModel() {

    private val articlesData = MutableLiveData<List<Article>>()

    fun subscribeArticles(): LiveData<List<Article>> = articlesData

    fun getArticles(categoryId: String) {
        apiRepository.getArticles(categoryId, object : ArticleRepository.LoadDataCallback<ArticleResponse> {
            override fun onDataLoaded(results: ArticleResponse) {
                articlesData.value = results.articles
            }

            override fun onDataNotAvailable() {
                articlesData.value = Collections.emptyList()
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        apiRepository.stopRequestArticles()
    }
}