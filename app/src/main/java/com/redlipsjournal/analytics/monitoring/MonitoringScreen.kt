package com.redlipsjournal.analytics.monitoring

import com.redlipsjournal.analytics.base.AnalyticsEvent
import java.io.Serializable

sealed class MonitoringScreen(
        private val screen: Screen,
        private val screenName: String = screen.toString()) : Serializable {

    protected val data: Map<String, String> = emptyMap()

    fun serialize(): AnalyticsEvent = AnalyticsEvent(AnalyticsEvent.Type.SCREEN, screenName, data)

    object SplashScreen : MonitoringScreen(Screen.SPLASH)
    object HomeScreen : MonitoringScreen(Screen.HOME)
    object FavoritesScreen : MonitoringScreen(Screen.FAVORITES)
    object ColumnsScreen : MonitoringScreen(Screen.COLUMNS)
    object ColumnDetail : MonitoringScreen(Screen.COLUMN_DETAIL)
    object ArticleDetail : MonitoringScreen(Screen.ARTICLE_DETAIL)
    object CategoriesDetail : MonitoringScreen(Screen.CATEGORIES_DETAIL)
}

enum class ScreenType(val id: String) {
    MODAL("modal"),
    POPUP("popup"),
    NOTIFICATION("notification");

    override fun toString() = id
}

enum class Flow(val id: String) {
    APPLICATIONS("applications");

    override fun toString() = id
}

enum class Screen(val id: String) {
    SPLASH("splash"),
    HOME("home"),
    FAVORITES("favorites"),
    COLUMNS("columns"),
    COLUMN_DETAIL("column_detail"),
    ARTICLE_DETAIL("article_detail"),
    CATEGORIES_DETAIL("categories_detail");

    override fun toString() = "screen_$id"
}