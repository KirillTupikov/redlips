package com.redlipsjournal.ui.base;

import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.redlipsjournal.R;
import com.redlipsjournal.analytics.AnalyticsManager;
import com.redlipsjournal.analytics.base.AnalyticsEvent;

public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar mToolbar;

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();

        if (getAnalyticsEvent() != null) {
            AnalyticsManager.INSTANCE.getInstance().track(getAnalyticsEvent());
        }
    }
    
    protected AnalyticsEvent getAnalyticsEvent() {
        return null;
    }

    protected void initViews() {
        setContentView(getLayoutId());
        initToolbar();
    }

    private void initToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbar.setTitleTextAppearance(this, R.style.TextAppearance_MontserratMedium_SemiLarge);
            setSupportActionBar(mToolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(true);
                if (getHomeIndicatorId() != -1) {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    actionBar.setHomeAsUpIndicator(getHomeIndicatorId());
                }
            }
        }
    }

    @DrawableRes
    protected int getHomeIndicatorId() {
        return -1;
    }

    protected void visibleToolBar(boolean isVisible) {
        findViewById(R.id.app_bar).setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    protected void setEnabledHome(boolean isEnable) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(isEnable);
            actionBar.setDisplayHomeAsUpEnabled(isEnable);
            actionBar.setDisplayShowHomeEnabled(isEnable);
        }
    }

    protected void setTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    protected void enableTitle(boolean isEnable) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(isEnable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

