package com.redlipsjournal.ui.home.column

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import com.redlipsjournal.R
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.ui.base.BaseActivity
import com.redlipsjournal.ui.home.category.CategoryFragment

class ColumnDetailActivity : BaseActivity() {

    override fun getLayoutId(): Int = R.layout.activity_column_detail

    companion object {

        private const val CATEGORY_POSITION = "category_position"

        fun start(context: Context, pos: Int) {
            val starter = Intent(context, ColumnDetailActivity::class.java)
            starter.putExtra(CATEGORY_POSITION, pos)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setEnabledHome(true)

        val pos = intent.getIntExtra(CATEGORY_POSITION, 0)

        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_container, ColumnContentFragment.newInstance(pos))
        ft.commit()
    }
}