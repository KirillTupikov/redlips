package com.redlipsjournal.utils

import android.content.Context

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

object HtmlHelper {

    private const val SCRIPT_TWITTER = "<script type=\"text/javascript\" src=\"http://platform.twitter.com/widgets.js\"></script>"
    private const val SCRIPT_INSTAGRAM = "<script async defer src=\"http://platform.instagram.com/en_US/embeds.js\"></script>"

    fun optimizedHtml(context: Context, html: String): String {
        val htmlNew = addBody(html)

        val doc = Jsoup.parse(htmlNew)

        optimizeIFrame(context, doc)
        optimizeImage(doc)

        return doc.html()
    }

    private fun optimizeIFrame(context: Context, doc: Document) {
        val elements = doc.select("iframe")

        val dm = context.resources.displayMetrics
        val widthPixels = dm.widthPixels

        for (element in elements) {

            if (!element.attr("src").isEmpty()) {
                if (element.attr("src").contains("youtube.com")) {

                    val h = ((widthPixels / dm.density) * 9) / 16 - 16 //16:9

                    element.attr("width", "100%")
                            .attr("height", h.toString())
                            .attr("max-width", "100%")
                            .attr("frameBorder", "0")
                            .attr("allowfullscreen", "true")
                } else
                    if (element.attr("src").contains("instagram.com")) {
                        val w = (widthPixels / dm.density) - 16

                        element.attr("width", w.toString())
                                .attr("height", (w * 2).toString())
                                .attr("max-width", "100%")
                                .attr("frameBorder", "0")
                    } else {
                        val w = (widthPixels / dm.density) - 16

                        element.attr("width", w.toString())
                                .attr("height", "auto")
                    }

            }
        }
    }

    private fun optimizeImage(doc: Document) {
        val elementsImg = doc.select("img")

        for (element in elementsImg) {
            element.attr("style", "")
                    .attr("width", "100%")
                    .attr("height", "auto")
                    .attr("max-width", "100%")
        }
    }

    private fun addBody(html: String): String {

        return "<html>" +
                "<head>" +
                "<style type=\"text/css\"> body {  padding: 0px; margin: 16px;} " +
                " figure {\n" +
                "    margin: 0 0 0 0;\n" +
                "}" +
                "</style>" +
                "</head>" +
                "<body>" +
                SCRIPT_TWITTER +
                SCRIPT_INSTAGRAM +
                html +
                "</body></html>"
    }
}
