package com.redlipsjournal.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.ui.base.BaseActivity
import com.redlipsjournal.ui.home.category.CategoryFragment
import com.redlipsjournal.ui.home.category.HomeFragment
import com.redlipsjournal.ui.home.column.ColumnFragment
import com.redlipsjournal.ui.home.favorites.FavoritesFragment
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener, MenuFragment.OnMenuClickListener {

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, HomeActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    private var mCurrentFragment: Fragment? = null

    private var mDrawerMenuFragment: MenuFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ThisApplication.instance?.appComponent?.inject(this)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        mDrawerMenuFragment = MenuFragment.newInstance()
        addFragment(mDrawerMenuFragment!!, R.id.fragment_drawer_menu)

//        onMenuClick(mMediaDataManager.categories!![0], 0)

        bottom_navigation.onNavigationItemSelectedListener = mNavListener
//        bottom_navigation.enableAnimation(false)
        bottom_navigation.selectedItemId = R.id.action_home
    }

    private val mNavListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        bottom_navigation.menu.setGroupCheckable(R.id.bottom_main_menu, true, true)
//        bottom_navigation.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_AUTO
        var title = resources.getString(R.string.app_name)
        when (item.itemId) {
            R.id.action_home -> {
                addFragment(HomeFragment.newInstance(), R.id.fragment_container)
                title = resources.getString(R.string.main_menu_home_title)
            }
            R.id.action_prefer -> {
                replaceFragment(FavoritesFragment.newInstance(), R.id.fragment_container)
                title = resources.getString(R.string.main_menu_prefer_title)
            }
            R.id.action_column -> {
                addFragment(ColumnFragment.newInstance(), R.id.fragment_container)
                title = resources.getString(R.string.main_menu_columns)
            }
//            R.id.action_shop -> addFragment(ShopFragment.newInstance(), R.id.fragment_container)
//            R.id.action_profile -> addFragment(ProfileFragment.newInstance(), R.id.fragment_container)
        }

        setTitle(title)

        (mDrawerMenuFragment as? OnMenuDisabledListener)?.uncheckedAll()

        true
    }


    private fun replaceFragment(fragment: Fragment, layoutId: Int) {
        val tag = fragment.javaClass.simpleName + HomeActivity::class.java.simpleName

        val currentFragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()

        if (currentFragment != null) {
            ft.remove(currentFragment)
        }

        hideAllFragments(ft);
        ft.add(layoutId, fragment, tag)
        ft.commit()
    }

    private fun hideAllFragments(ft: FragmentTransaction) {
        for (fragmentItem: Fragment? in supportFragmentManager.fragments) {
            if (fragmentItem != null && !fragmentItem.isHidden && fragmentItem !is MenuFragment) {
                ft.hide(fragmentItem)
            }
        }
    }

    private fun addFragment(fragment: Fragment, layoutId: Int, category: Category? = null) {

        val tag = fragment.javaClass.simpleName + HomeActivity::class.java.simpleName
        val currentFragment = supportFragmentManager.findFragmentByTag(tag)
        val ft = supportFragmentManager.beginTransaction()

        hideAllFragments(ft)

        if (currentFragment == null) {
            ft.add(layoutId, fragment, tag)
        } else {
            if (category != null && currentFragment is CategoryFragment) {
                currentFragment.setCategory(category)
            }
            ft.show(currentFragment)
        }
        ft.commit()
    }

    override fun onMenuClick(category: Category, position: Int) {
        drawer_layout.closeDrawers()

        setTitle(category.title)
        bottom_navigation.menu.setGroupCheckable(R.id.bottom_main_menu, false, true)
//        bottom_navigation.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED

        addFragment(CategoryFragment.newInstance(position), R.id.fragment_container, category)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {

            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
