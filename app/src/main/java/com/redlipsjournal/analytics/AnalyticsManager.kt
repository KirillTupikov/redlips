package com.redlipsjournal.analytics

import com.redlipsjournal.analytics.base.Analytics

object AnalyticsManager {
    fun init(analytic: Analytics) {
        AnalyticsManager.analytic = analytic
    }

    fun getInstance(): Analytics? {
        return analytic
    }

    private var analytic: Analytics? = null
}