package com.redlipsjournal.analytics.firebase

import android.app.Activity
import android.os.Bundle
import com.redlipsjournal.analytics.base.MonitoringDriver
import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseDriverImpl(private val firebase: FirebaseAnalytics) : MonitoringDriver {

    override fun screen(activity: Activity, screen: String, data: Map<String, String>) {
        firebase.setCurrentScreen(activity, screen, null)
    }

    override fun screen(screen: String, data: Map<String, String>) {
        event(screen, mapOf(FirebaseAnalytics.Param.ITEM_NAME to screen))
    }

    override fun action(action: String, data: Map<String, String>) {

    }

    override fun event(event: String, data: Map<String, String>) {
        if (data.isEmpty()) {
            firebase.logEvent(event, null)
        } else {
            val bundle = Bundle()
            data.forEach { bundle.putString(it.key, it.value) }
            firebase.logEvent(event, bundle)
        }
    }
}