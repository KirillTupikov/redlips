package com.redlipsjournal.ui.home.shop

import android.os.Bundle
import com.redlipsjournal.R
import com.redlipsjournal.ui.base.BaseFragment

class ShopFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_shop
    }

    companion object {

        fun newInstance(): ShopFragment {

            val args = Bundle()

            val fragment = ShopFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
