package com.redlipsjournal.ui.home.column.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.redlipsjournal.R
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.ui.base.BaseRecyclerViewAdapter
import com.redlipsjournal.ui.home.column.ColumnDetailActivity

class ColumnAdapter : BaseRecyclerViewAdapter<Category, ColumnAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColumnAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.column_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val column = mData[position]

        column.author?.displayName?.let { author ->
            holder.author.text = author
        }

        column.title?.let { title ->
            holder.title.text = title
        }

        column.articlesCount?.let { count ->
            holder.count.text = count.toString()
        }

        holder.itemView.setOnClickListener { view ->
            ColumnDetailActivity.start(view.context, position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val author: TextView = itemView.findViewById(R.id.author)
        val title: TextView = itemView.findViewById(R.id.title)
        val count: TextView = itemView.findViewById(R.id.article_count)
    }
}