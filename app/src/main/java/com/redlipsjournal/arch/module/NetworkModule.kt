package com.redlipsjournal.arch.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.api.ApiService
import com.redlipsjournal.arch.repositories.ArticleRepository
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.managers.FavoritesManager
import com.redlipsjournal.utils.NetworkHelper
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.internal.cache.CacheInterceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        const val BASE_URL = "https://redlipsjournal.com"
    }

    @Provides
    @Singleton
    internal fun provideApplicationViewModelFactory(apiRepository: ArticleRepository, favoritesManager: FavoritesManager) = ApplicationViewModelFactory(apiRepository, favoritesManager)

    @Provides
    @Singleton
    fun provideMediaDataManager(): MediaDataManager {
        return MediaDataManager()
    }

    @Provides
    @Singleton
    fun providePreferencesManager(): FavoritesManager {
        return FavoritesManager()
    }

    @Provides
    @Singleton
    fun provideArticleRepository(ssoApiManager: ApiService, gson: Gson): ArticleRepository = ArticleRepository(ssoApiManager, gson)

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    fun provideHttpClient(interceptor: HttpLoggingInterceptor, checkInternetInterceptor: Interceptor, cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .addInterceptor(checkInternetInterceptor)
                .build()
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    fun provideCheckInternetInterceptor(): Interceptor {
        return Interceptor { chain ->

            // Get the request from the chain.
            var request = chain.request()

            /*
            *  Leveraging the advantage of using Kotlin,
            *  we initialize the request and change its header depending on whether
            *  the device is connected to Internet or not.
            */
            request = if (NetworkHelper.hasNetwork())
            /*
            *  If there is Internet, get the cache that was stored 5 seconds ago.
            *  If the cache is older than 5 seconds, then discard it,
            *  and indicate an error in fetching the response.
            *  The 'max-age' attribute is responsible for this behavior.
            */
                request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
            else
            /*
            *  If there is no Internet, get the cache that was stored 7 days ago.
            *  If the cache is older than 7 days, then discard it,
            *  and indicate an error in fetching the response.
            *  The 'max-stale' attribute is responsible for this behavior.
            *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
            */
                request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
            // End of if-else statement

            // Add the modified request to the chain.
            chain.proceed(request)

        }
    }

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideCache(): Cache {
        val httpCacheDir = File(ThisApplication.instance?.cacheDir, "redlips")
        val cacheSize = 40 * 1024 * 1024 // 40 Mb
        return Cache(httpCacheDir, cacheSize.toLong())
    }
}