package com.redlipsjournal.ui.home.favorites

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.PreferencesViewModel
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.managers.FavoritesManager
import com.redlipsjournal.ui.base.BaseFragment
import com.redlipsjournal.ui.home.category.ArticleAdapter
import kotlinx.android.synthetic.main.fragment_prefer.*
import javax.inject.Inject

class FavoritesFragment : BaseFragment() {

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    @Inject
    lateinit var mFavoritesManager: FavoritesManager

    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mFavoritesViewModel: PreferencesViewModel

    override fun getLayoutId(): Int {
        return R.layout.fragment_prefer
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.FavoritesScreen.serialize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThisApplication.instance?.appComponent?.inject(this)

        mFavoritesViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(PreferencesViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mFavoritesViewModel.subscribePreferences().observe(this, Observer { articles ->

            if (recyclerView.adapter == null) {
                val adapter = ArticleAdapter(mFavoritesManager, mMediaDataManager)
                activity?.let { adapter.setActivity(it) }
                adapter.setFavoritesListener(object : ArticleAdapter.FavoritesClickListener {
                    override fun onClick(itemId: String, isChecked: Boolean) {
                        //TODO will add logic for delete item
                        getFavorites()
                    }
                })

                recyclerView.adapter = adapter
            }

            if (articles?.isNotEmpty() == true) {
                (recyclerView.adapter as ArticleAdapter).setData(articles)
                showContent()
            } else {
                showNoData()
            }
        })

        getFavorites()
    }

    private fun getFavorites() {
        showProgress()
        mFavoritesViewModel.getArticles()
    }

    private fun showProgress() {
        viewFlipper.displayedChild = 0
    }

    private fun showContent() {
        viewFlipper.displayedChild = 1
    }

    private fun showNoData() {
        viewFlipper.displayedChild = 2
    }

    companion object {

        fun newInstance(): FavoritesFragment {

            val args = Bundle()

            val fragment = FavoritesFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
