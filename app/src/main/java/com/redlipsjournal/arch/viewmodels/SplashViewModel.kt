package com.redlipsjournal.arch.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.arch.repositories.ArticleRepository
import java.util.*

class SplashViewModel(private val apiRepository: ArticleRepository) : ViewModel() {

    private val categoryData = MutableLiveData<List<Category>>()

    fun subscribeCategory(): LiveData<List<Category>> = categoryData

    fun getCategory() {
        apiRepository.getCategories(object : ArticleRepository.LoadDataCallback<List<Category>> {
            override fun onDataLoaded(results: List<Category>) {
                categoryData.value = results
            }

            override fun onDataNotAvailable() {
                categoryData.value = Collections.emptyList()
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        apiRepository.stopRequestCategories()
    }
}