package com.redlipsjournal.analytics.base

import android.app.Activity

interface Analytics {
    fun screen(activity: Activity, screen: String, data: Map<String, String>)
    fun screen(screen: String, data: Map<String, String>)
    fun action(action: String, data: Map<String, String>)
    fun event(event: String, data: Map<String, String>)

    fun track(activity: Activity, event: AnalyticsEvent) {
        when (event.type) {
            AnalyticsEvent.Type.SCREEN -> this.screen(activity, event.id, event.payload)
            else -> track(event)
        }
    }

    fun track(event: AnalyticsEvent) {
        when (event.type) {
            AnalyticsEvent.Type.SCREEN -> this.screen(event.id, event.payload)
            AnalyticsEvent.Type.ACTION -> this.action(event.id, event.payload)
            AnalyticsEvent.Type.EVENT -> this.event(event.id, event.payload)
        }
    }
}