package com.redlipsjournal.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.redlipsjournal.ThisApplication

object NetworkHelper {

    fun hasNetwork(): Boolean {
        var isConnected = false
        val connectivityManager = ThisApplication.instance?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            isConnected = true
        }

        return isConnected
    }
}
