package com.redlipsjournal.ui.home.category

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.redlipsjournal.R
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.arch.viewmodels.ApplicationViewModelFactory
import com.redlipsjournal.arch.viewmodels.CategoryViewModel
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.managers.FavoritesManager
import com.redlipsjournal.ui.base.BaseReceiverFragment
import kotlinx.android.synthetic.main.fragment_category.*
import java.util.*
import javax.inject.Inject

open class CategoryFragment : BaseReceiverFragment() {

    @Inject
    lateinit var mMediaDataManager: MediaDataManager

    @Inject
    lateinit var mFavoritesManager: FavoritesManager

    @Inject
    lateinit var mViewModelFactory: ApplicationViewModelFactory
    private lateinit var mCategoryViewModel: CategoryViewModel

    private var mCurrentCategory: Category? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_category
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.CategoriesDetail.serialize()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ThisApplication.instance?.appComponent?.inject(this)

        mCategoryViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(CategoryViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val position: Int? = arguments?.getInt(EXTRA_CATEGORY_POSITION, 0)
        mCurrentCategory = getCategory(position)

        mCategoryViewModel.subscribeArticles().observe(this, Observer { articles ->
            val dataList = ArrayList<Any>()

            mCurrentCategory?.let { dataList.add(it) }
            articles?.let { dataList.addAll(it) }

            if (recyclerView.adapter == null) {
                val adapter = ArticleAdapter(mFavoritesManager, mMediaDataManager)
                activity?.let { adapter.setActivity(it) }
                recyclerView.adapter = adapter
            }

            (recyclerView.adapter as ArticleAdapter).setData(dataList)
            if (dataList.isNotEmpty()) {
                showContent()
            }
        })

        getArticles()
    }

    fun setCategory(category: Category) {
        mCurrentCategory = category
        recyclerView.layoutManager?.scrollToPosition(0)
        getArticles()
    }

    private fun getArticles() {
        showProgress()
        mCurrentCategory?.id?.let { mCategoryViewModel.getArticles(it) }
    }

    private fun showProgress() {
        viewFlipper?.displayedChild = 0
    }

    private fun showContent() {
        viewFlipper.displayedChild = 1
    }

    protected open fun getCategory(position: Int?) = position?.let { pos -> mMediaDataManager.getCategoryByPosition(pos) }


    override fun onConnectionChanged(isConnection: Boolean) {
        recyclerView.adapter?.let { adapter ->
            if (adapter.itemCount > 0) {
                val toast = Toast.makeText(context, getString(if (isConnection) R.string.found_connection
                else R.string.connection_lost), Toast.LENGTH_SHORT)

                val styledAttributes = context?.theme?.obtainStyledAttributes(
                        intArrayOf(android.R.attr.actionBarSize))

                toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0,
                        styledAttributes?.getDimension(0, 0f)?.toInt() ?: 0)

                toast.show()
                styledAttributes?.recycle()
            } else if (isConnection) {
                getArticles()
            } else {
                viewFlipper.displayedChild = 2
            }
        }
    }

    companion object {

        const val EXTRA_CATEGORY_POSITION = "category_position"

        fun newInstance(position: Int): CategoryFragment {

            val args = Bundle()
            args.putInt(EXTRA_CATEGORY_POSITION, position)

            val fragment = CategoryFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
