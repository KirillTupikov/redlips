package com.redlipsjournal.arch.components

import com.redlipsjournal.ThisApplication
import com.redlipsjournal.arch.module.ApplicationModule
import com.redlipsjournal.arch.module.NetworkModule
import com.redlipsjournal.arch.repositories.ArticleRepository
import com.redlipsjournal.ui.home.HomeActivity
import com.redlipsjournal.ui.SplashActivity
import com.redlipsjournal.ui.home.category.CategoryFragment
import com.redlipsjournal.ui.home.MenuFragment
import com.redlipsjournal.ui.home.category.ArticleDetailActivity
import com.redlipsjournal.ui.home.category.HomeFragment
import com.redlipsjournal.ui.home.column.ColumnFragment
import com.redlipsjournal.ui.home.favorites.FavoritesFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (NetworkModule::class)])
interface AppComponent {

    fun inject(thisApplication: ThisApplication)

    fun inject(homeActivity: HomeActivity)

    fun inject(apiRepository: ArticleRepository)

    fun inject(splashActivity: SplashActivity)

    fun inject(menuFragment: MenuFragment)

    fun inject(categoryFragment: CategoryFragment)

    fun inject(homeFragment: HomeFragment)

    fun inject(articleDetailActivity: ArticleDetailActivity)

    fun inject(columnFragment: ColumnFragment)

    fun inject(favoritesFragment: FavoritesFragment)

}