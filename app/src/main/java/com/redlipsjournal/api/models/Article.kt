package com.redlipsjournal.api.models

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

class Article {

    @SerializedName(ID)
    var id: String? = null
    @SerializedName(TAGS)
    val tags: Array<String>? = null
    @SerializedName(EDIT_URL)
    val editUrl: String? = null
    @SerializedName(POST_STATUS)
    val postStatus: String? = null
    @SerializedName(FEATURED)
    val featured: String? = null
    @SerializedName(PLACES)
    val places: Array<String>? = null
    @SerializedName(ARTICLE_TYPE)
    val articleType: String? = null
    @SerializedName(FEEDBACK_QUESTION)
    val feedbackQuestion: String? = null
    @SerializedName(META_KEYWORD)
    val metaKeyword: String? = null
    @SerializedName(FEEDBACK_ENABLED)
    val feedbackEnabled: String? = null
    @SerializedName(POST_VIEW)
    var postViews: String? = null
    @SerializedName(URL)
    val url: String? = null
    @SerializedName(AUTHOR)
    var author: Author? = null
    @SerializedName(CONTENT_HTML)
    val contentHtml: String? = null
    @SerializedName(TITLE)
    var title: String? = null
    @SerializedName(META_DESCRIPTION)
    val metaDescription: String? = null
    @SerializedName(POST_THUMBNAIL)
    var postThumbnail: PostThumbnail? = null
    @SerializedName(POPULAR)
    val popular: String? = null
    @SerializedName(CATEGORIES)
    val categories: Array<String>? = null
    @SerializedName(HTML)
    val html: String? = null
    @SerializedName(ARTICLE_SOURCE_URL)
    val articleSourceUrl: String? = null
    @SerializedName(SPONSORED)
    val sponsored: String? = null
    @SerializedName(POST_NAME)
    val postName: String? = null
    @SerializedName(POST_DATE)
    val postDate: String? = null

    companion object {

        const val ID = "id"
        const val TAGS = "tags"
        const val EDIT_URL = "edit_url"
        const val POST_STATUS = "post_status"
        const val FEATURED = "featured"
        const val PLACES = "places"
        const val ARTICLE_TYPE = "article_type"
        const val FEEDBACK_QUESTION = "feedback_question"
        const val META_KEYWORD = "meta_keyword"
        const val FEEDBACK_ENABLED = "feedback_enabled"
        const val POST_VIEW = "post_views"
        const val URL = "url"
        const val AUTHOR = "author"
        const val CONTENT_HTML = "content_html"
        const val TITLE = "title"
        const val META_DESCRIPTION = "meta_description"
        const val POST_THUMBNAIL = "post_thumbnail"
        const val POPULAR = "popular"
        const val CATEGORIES = "categories"
        const val HTML = "html"
        const val ARTICLE_SOURCE_URL = "article_source_url"
        const val SPONSORED = "sponsored"
        const val POST_NAME = "post_name"
        const val POST_DATE = "post_date"
    }

    constructor(jsonObject: JSONObject) {

        if (jsonObject.has(ID)) {
            id = jsonObject.optString(ID)
        }
        if (jsonObject.has(TITLE)) {
            title = jsonObject.optString(TITLE)
        }
        if (jsonObject.has(POST_VIEW)) {
            postViews = jsonObject.optString(POST_VIEW)
        }
        if (jsonObject.has(Author.DISPLAY_NAME)) {
            val newAuthor = Author()
            newAuthor.displayName = jsonObject.optString(Author.DISPLAY_NAME)
            author = newAuthor
        }
        if (jsonObject.has(PostThumbnail.FILE)) {
            val newPostThumbnail = PostThumbnail()
            newPostThumbnail.file = jsonObject.optString(PostThumbnail.FILE)
            postThumbnail = newPostThumbnail
        }

    }

    override fun toString(): String {
        return title.toString()
    }
}
