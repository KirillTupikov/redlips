package com.redlipsjournal.analytics.base

import android.app.Activity

interface MonitoringDriver {
    fun screen(activity: Activity, screen: String, data: Map<String, String>)
    fun screen(screen: String, data: Map<String, String>)
    fun action(action: String, data: Map<String, String>)
    fun event(event: String, data: Map<String, String>)
}