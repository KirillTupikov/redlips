package com.redlipsjournal.managers

import android.text.TextUtils
import com.redlipsjournal.ThisApplication
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.Author
import com.redlipsjournal.api.models.PostThumbnail
import com.redlipsjournal.utils.EventBusHelper
import org.greenrobot.eventbus.EventBus
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class FavoritesManager {

    companion object {

        const val PREFERENCES = "PREFERENCES"
        const val CATEGORIES_IDS = "CATEGORIES_IDS"
    }

    private val prefs = ThisApplication.instance?.getSharedPreferences(PREFERENCES, 0)

    fun addArticle(article: Article) {

        val idsArray = getFavorites()
        val jsonObject = JSONObject()
        jsonObject.put(Article.ID, article.id)
        jsonObject.put(Article.TITLE, article.title)
        jsonObject.put(Author.DISPLAY_NAME, article.author?.displayName)
        jsonObject.put(Article.POST_VIEW, article.postViews)
        jsonObject.put(PostThumbnail.FILE, article.postThumbnail?.file.toString())
        idsArray.put(jsonObject)

        val editor = prefs!!.edit()
        editor.putString(CATEGORIES_IDS, idsArray.toString())
        editor.apply()

        EventBus.getDefault()
                .post(EventBusHelper.PreferenceChangedEvent())
    }

    fun removeArticleById(id: String) {
        val idsArray = getFavorites()

        for (i in 0 until idsArray.length()) {
            try {
                val jsonObject = idsArray.optJSONObject(i)
                if (TextUtils.equals(jsonObject.optString(Article.ID), id)) {
                    idsArray.remove(i)
                    break
                }
            } catch (ex: Exception) {
            }
        }

        val editor = prefs!!.edit()
        editor.putString(CATEGORIES_IDS, idsArray.toString())
        editor.apply()

        EventBus.getDefault()
                .post(EventBusHelper.PreferenceChangedEvent())
    }

    fun isInPreferences(id: String?): Boolean {
        val idsArray = getFavorites()
        for (i in 0 until idsArray.length()) {
            try {
                val jsonObject = idsArray.optJSONObject(i)
                if (TextUtils.equals(jsonObject.optString(Article.ID), id))
                    return true
            } catch (ex: Exception) {
                //Do nothing
            }
        }
        return false
    }

    private fun getFavorites(): JSONArray {
        val s = prefs?.getString(CATEGORIES_IDS, "")
        var jsonArray = JSONArray()
        try {
            jsonArray = JSONArray(s)
        } catch (ex: Exception) {
            //Do nothing
        }
        return jsonArray
    }

    fun getArticlesList(): MutableList<Article> {
        val jsonArray = getFavorites()
        val articlesList = mutableListOf<Article>()
        try {
            for (i in jsonArray.length() downTo  0) {
                val jsonObject = jsonArray.optJSONObject(i)
                if (jsonObject != null) {
                    articlesList.add(Article(jsonObject))
                }
            }
        } catch (ex: Exception) {
            //Do nothing
        }

        return articlesList
    }
}