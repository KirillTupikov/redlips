package com.redlipsjournal.managers

import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.Category

class MediaDataManager {

    var selectArticle: Article? = null

    var categories: List<Category>? = null

    var columns: List<Category>? = null

    fun getCategoryByPosition(position: Int): Category? {
        return if (categories != null && categories!!.size > position) {
            categories!![position]
        } else null
    }

    fun getColumnByPosition(position: Int): Category? {
        return if (columns != null && columns!!.size > position) {
            columns!![position]
        } else null
    }


}
