package com.redlipsjournal.ui.home.column

import android.os.Bundle
import com.redlipsjournal.analytics.base.AnalyticsEvent
import com.redlipsjournal.analytics.monitoring.MonitoringScreen
import com.redlipsjournal.ui.home.category.CategoryFragment

class ColumnContentFragment : CategoryFragment() {

    companion object {

        private const val EXTRA_CATEGORY_POSITION = "category_position"

        fun newInstance(position: Int): ColumnContentFragment {

            val args = Bundle()
            args.putInt(EXTRA_CATEGORY_POSITION, position)

            val fragment = ColumnContentFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getAnalyticsEvent(): AnalyticsEvent? {
        return MonitoringScreen.ColumnDetail.serialize()
    }

    override fun getCategory(position: Int?) = position?.let { pos -> mMediaDataManager.getColumnByPosition(pos) }
}