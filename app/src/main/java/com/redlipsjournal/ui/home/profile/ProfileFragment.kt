package com.redlipsjournal.ui.home.profile

import android.os.Bundle
import com.redlipsjournal.R
import com.redlipsjournal.ui.base.BaseFragment

class ProfileFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_profile
    }

    companion object {

        fun newInstance(): ProfileFragment {

            val args = Bundle()

            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
