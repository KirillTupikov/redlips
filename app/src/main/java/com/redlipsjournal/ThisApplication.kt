package com.redlipsjournal

import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.onesignal.OneSignal
import com.redlipsjournal.analytics.AnalyticsManager
import com.redlipsjournal.analytics.base.AnalyticsImpl
import com.redlipsjournal.analytics.firebase.FirebaseDriverImpl
import com.redlipsjournal.arch.components.AppComponent
import com.redlipsjournal.arch.components.DaggerAppComponent
import com.redlipsjournal.arch.module.ApplicationModule
import com.redlipsjournal.arch.module.NetworkModule
import io.fabric.sdk.android.Fabric

class ThisApplication : MultiDexApplication() {

    var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        appComponent = DaggerAppComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .networkModule(NetworkModule())
                .build()

        val firebase = FirebaseAnalytics.getInstance(this)
        val firebaseDriver = FirebaseDriverImpl(firebase)

        AnalyticsManager.init(AnalyticsImpl(firebaseDriver))

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()

        instance = this
    }

    companion object {

        var instance: ThisApplication? = null
            private set
    }
}
