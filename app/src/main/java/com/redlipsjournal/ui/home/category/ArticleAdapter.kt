package com.redlipsjournal.ui.home.category

import android.app.Activity
import android.content.Intent
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.redlipsjournal.R
import com.redlipsjournal.analytics.AnalyticsManager
import com.redlipsjournal.analytics.monitoring.MonitoringEvent
import com.redlipsjournal.api.models.Article
import com.redlipsjournal.api.models.Category
import com.redlipsjournal.managers.MediaDataManager
import com.redlipsjournal.managers.FavoritesManager

import com.redlipsjournal.ui.base.BaseRecyclerViewAdapter
import com.redlipsjournal.ui.home.category.ArticleDetailActivity.Companion.AUTHOR_TRANSITION_NAME
import com.redlipsjournal.ui.home.category.ArticleDetailActivity.Companion.CONTAINER_TRANSITION_NAME
import com.redlipsjournal.ui.home.category.ArticleDetailActivity.Companion.COUNT_TRANSITION_NAME
import com.redlipsjournal.ui.home.category.ArticleDetailActivity.Companion.IMAGE_TRANSITION_NAME
import com.redlipsjournal.ui.home.category.ArticleDetailActivity.Companion.TITLE_TRANSITION_NAME
import com.redlipsjournal.utils.ImageHelper

class ArticleAdapter(private val favoritesManager: FavoritesManager,
                     private val mediaManager: MediaDataManager) : BaseRecyclerViewAdapter<Any, RecyclerView.ViewHolder>() {

    companion object {
        const val HEADER_TYPE = 1
        const val ARTICLE_TYPE = 2
    }

    private var favoritesListener: FavoritesClickListener? = null

    private var mActivity: Activity? = null

    fun setActivity(activity: Activity) {
        mActivity = activity
    }

    fun setFavoritesListener(favoritesListener: FavoritesClickListener) {
        this.favoritesListener = favoritesListener
    }

    override fun getItemViewType(position: Int): Int {
        if (mData[position] is Category) {
            return HEADER_TYPE
        }
        return ARTICLE_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            HEADER_TYPE -> {
                val view = inflater.inflate(R.layout.item_category_header_view, parent, false)
                return HeaderViewHolder(view)
            }
            ARTICLE_TYPE -> {
                val view = inflater.inflate(R.layout.item_article_view, parent, false)
                return ArticleViewHolder(view)
            }
        }

        throw IllegalArgumentException("Bad item type")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val context = holder.itemView.context
        when (getItemViewType(position)) {
            HEADER_TYPE -> {
                val category: Category = mData[position] as Category
                val headerViewHolder: HeaderViewHolder = holder as HeaderViewHolder

                headerViewHolder.name.text = category.title
                headerViewHolder.author.text = category.author?.displayName
                headerViewHolder.description.text = category.description
            }

            ARTICLE_TYPE -> {
                val article: Article = mData[position] as Article
                val articleViewHolder: ArticleViewHolder = holder as ArticleViewHolder

                articleViewHolder.name.text = article.title
                articleViewHolder.author.text = article.author?.displayName
                articleViewHolder.count.text = article.postViews

                ImageHelper.loadImageWithPlaceholder(articleViewHolder.image, article.postThumbnail?.file.toString())

                ViewCompat.setTransitionName(articleViewHolder.image, article.title + "Image")
                ViewCompat.setTransitionName(articleViewHolder.name, article.title + "Name")
                ViewCompat.setTransitionName(articleViewHolder.author, article.title + "Author")
                ViewCompat.setTransitionName(articleViewHolder.count, article.title + "Count")
                ViewCompat.setTransitionName(articleViewHolder.container, article.title + "Container")

                articleViewHolder.preferencesCheckbox.isChecked = favoritesManager.isInPreferences(article.id)

                articleViewHolder.preferencesCheckbox.setOnClickListener {
                    article.id?.let { id ->
                        if (articleViewHolder.preferencesCheckbox.isChecked) {
                            AnalyticsManager.getInstance()?.track(MonitoringEvent.ClickAddFavorites.serialize())
                            favoritesManager.addArticle(article)
                        } else {
                            AnalyticsManager.getInstance()?.track(MonitoringEvent.ClickRemoveFavorites.serialize())
                            favoritesManager.removeArticleById(id)
                        }
                    }

                    favoritesListener?.onClick(article.id.toString(),
                            articleViewHolder.preferencesCheckbox.isChecked)
                }

                articleViewHolder.itemView.setOnClickListener {
                    mediaManager.selectArticle = article

                    val intent = Intent(context, ArticleDetailActivity::class.java)
                    intent.putExtra(IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(articleViewHolder.image))
                    intent.putExtra(TITLE_TRANSITION_NAME, ViewCompat.getTransitionName(articleViewHolder.name))
                    intent.putExtra(AUTHOR_TRANSITION_NAME, ViewCompat.getTransitionName(articleViewHolder.author))
                    intent.putExtra(COUNT_TRANSITION_NAME, ViewCompat.getTransitionName(articleViewHolder.count))
                    intent.putExtra(CONTAINER_TRANSITION_NAME, ViewCompat.getTransitionName(articleViewHolder.container))

                    val p1 = Pair.create<View, String>(articleViewHolder.image,
                            ViewCompat.getTransitionName(articleViewHolder.image))
                    val p2 = Pair.create<View, String>(articleViewHolder.name,
                            ViewCompat.getTransitionName(articleViewHolder.name))
                    val p3 = Pair.create<View, String>(articleViewHolder.author,
                            ViewCompat.getTransitionName(articleViewHolder.author))
                    val p4 = Pair.create<View, String>(articleViewHolder.count,
                            ViewCompat.getTransitionName(articleViewHolder.count))
                    val p5 = Pair.create<View, String>(articleViewHolder.container,
                            ViewCompat.getTransitionName(articleViewHolder.container))

                    val options = mActivity?.let { activity ->
                        ActivityOptionsCompat.makeSceneTransitionAnimation(
                                activity, p1, p2, p3, p4, p5)
                    }

                    context.startActivity(intent, options?.toBundle())
                }
            }
        }
    }

    interface FavoritesClickListener {
        fun onClick(itemId: String, isChecked: Boolean)
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val name: TextView = view.findViewById(R.id.category_name)
        val description: TextView = view.findViewById(R.id.description)
        val author: TextView = view.findViewById(R.id.author)
    }

    inner class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val name: TextView = view.findViewById(R.id.article_name)
        val preferencesCheckbox: CheckBox = view.findViewById(R.id.favorites_button)
        val image: ImageView = view.findViewById(R.id.article_image)
        val author: TextView = view.findViewById(R.id.author)
        val count: TextView = view.findViewById(R.id.count_view)
        val container: View = view.findViewById(R.id.container_view)
    }
}
